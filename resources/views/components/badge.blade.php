@props(['textColor', 'bgColor'])

@php
    $textColor = match ($textColor) {
        'white' => 'text-white',
        default => 'text-gray-800',
    };

    $bgColor = match ($bgColor) {
        'violet' => 'bg-violet-400',
        'fuchsia' => 'bg-fuchsia-400',
        'rose' => 'bg-rose-400',
        'teal' => 'bg-teal-400',
        'amber' => 'bg-amber-400',
        default => 'bg-gray-100',
    };
@endphp

<button {{ $attributes }} class="{{ $textColor }} {{ $bgColor }} rounded-xl px-3 py-1 text-base">
    {{ $slot }} </button>
