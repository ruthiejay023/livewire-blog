@props(['author', 'size'])

@php
    $imageSize = match ($size) {
        'sm' => 'w-7 h-7',
        'md' => 'w-10 h-10',
        'lg' => 'w-14 h-14',
        default => 'w-10 h-10',
    };

    $textSize = match ($size) {
        'sm' => 'text-xs',
        'md' => 'text-base',
        'lg' => 'text-lg',
        default => 'text-sm',
    };
@endphp
<img class="{{ $imageSize }} rounded-full mr-3" src="{{ $author->profile_photo_url }}" alt="{{ $author->name }}">
<span class="mr-1 {{ $textSize }}">{{ $author->name }}</span>
