<?php

namespace App\Livewire;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Support\Facades\Log;
use Livewire\Attributes\Computed;
use Livewire\Attributes\On;
use Livewire\Attributes\Url;
use Livewire\Component;
use Livewire\WithPagination;

class PostList extends Component
{
    use WithPagination;
    #[Url()]
    public $sort = 'desc';

    #[Url()]
    public $search = '';

    #[Url()]
    public $category = '';

    #[Url()]
    public $popular = false;

    public function setSort($sort)
    {
        $this->sort = ($sort === 'desc') ? 'desc' : 'asc';
    }

    #[On('search')]
    public function updateSearch($search)
    {
        $this->search = $search;
    }

    public function clearSearch()
    {
        $this->search = '';
    }

    #[Computed()]
    public function posts()
    {
        Log::info('Fetching posts', [
            'sort' => $this->sort,
            'search' => $this->search,
            'category' => $this->category,
            'popular' => $this->popular,
        ]);

        $query = Post::published()
            ->when($this->category, function ($query) {
                $query->withCategory($this->category);
            })
            ->when($this->popular, function ($query) {
                $query->popular();
            })
            ->where('title', 'like', "%{$this->search}%")
            ->orderBy('published_at', $this->sort);
        return $query->paginate(5);
    }

    public function render()
    {
        return view('livewire.post-list');
    }
}
